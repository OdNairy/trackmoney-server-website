INSERT INTO `TrackMoney`.`users` (`username`, `password`, `enabled`, `email`) VALUES ('OdNairy', 'a', 1, 'od@od.com');
INSERT INTO `TrackMoney`.`users` (`username`, `password`, `enabled`, `email`) VALUES ('a', 'a', 1, 'a@a.o');

INSERT INTO `TrackMoney`.`authorities` (`authority_id`, `username`, `authority`) VALUES (1, 'OdNairy', 'ROLE_USER');
INSERT INTO `TrackMoney`.`authorities` (`authority_id`, `username`, `authority`) VALUES (2, 'OdNairy', 'ROLE_ADMIN');
INSERT INTO `TrackMoney`.`authorities` (`authority_id`, `username`, `authority`) VALUES (3, 'a', 'ROLE_USER');

INSERT INTO `TrackMoney`.`category` (`category_id`, `title`) VALUES (1, 'Income');
INSERT INTO `TrackMoney`.`category` (`category_id`, `title`) VALUES (2, 'Charges');

INSERT INTO `TrackMoney`.`operation` (`operation_id`, `money`, `comment`, `users_username`, `time`) VALUES (1, 15000, 'Зарплата', 'OdNairy', '01/01/2012');
INSERT INTO `TrackMoney`.`operation` (`operation_id`, `money`, `comment`, `users_username`, `time`) VALUES (2, 800, 'Халтура', 'OdNairy', '02/01/2012');
INSERT INTO `TrackMoney`.`operation` (`operation_id`, `money`, `comment`, `users_username`, `time`) VALUES (3, -240, 'Еда', 'OdNairy', '02/01/2012');
