<%--
  Created by IntelliJ IDEA.
  User: odnairy
  Date: 5/31/12
  Time: 5:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="template.jsp"/>
<html>
<head>
    <title></title>
</head>
<body>

<div id="container">
    <div id="inner">

        <div id="centerLeft">
            <c:if test="${!empty addresses}">
                <table class="data">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>

                    </tr>
                    <c:forEach items="${addresses}" var="address">
                        <tr>
                            <td>${address.addresseeId}</td>
                            <td>${address.addresseeName}</td>

                            <td><a href="addresses/delete/${address.addresseeId}"><spring:message
                                    code="label.delete"/></a></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>
            <form:form method="post" action="addresses/add" commandName="addressee">
                <table>
                    <tr class="headerRow">
                        <th class="IdHeaderCell">Id</th>
                        <th>Name</th>
                    </tr>
                    <tr>
                        <td></td>
                        <td><form:input path="addresseeName"/></td>
                    </tr>
                </table>
            </form:form>
        </div>
    </div>
</div>

</body>
</html>