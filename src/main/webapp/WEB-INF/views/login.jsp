<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link rel="stylesheet" type="text/css" href="resources/css/demo.css"/>
<link rel="stylesheet" type="text/css" href="resources/css/style3.css"/>
<link rel="stylesheet" type="text/css" href="resources/css/animate-custom.css"/>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <title><spring:message code="label.title"/></title>
</head>
<body style="width: 40%;margin-left: 30%;">

<c:if test="${not empty param.error}    ">
    <font color="red"> <spring:message code="label.loginerror"/>
        : ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message} </font>
</c:if>
<div class="container">
    <section>
        <div id="container_demo">
            <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>

            <div id="wrapper">
                <div id="login" class="animate form">
                    <form method="POST" action="<c:url value="/j_spring_security_check" />">
                        <h1>Log in</h1>

                        <p>
                            <label for="username" class="uname" data-icon="u"> <spring:message
                                    code="label.login.UserLogin"/> </label>
                            <input id="username" name="j_username" required="required" type="text"
                                   placeholder="myusername or mymail@mail.com"/>
                        </p>

                        <p>
                            <label for="password" class="youpasswd" data-icon="p"> <spring:message
                                    code="label.login.UserPassword"/> </label>
                            <input id="password" name="j_password" required="required" type="password"
                                   placeholder="eg. X8df!90EO"/>
                        </p>

                        <p class="keeplogin">
                            <input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping"
                                   checked="checked"/>
                            <label for="loginkeeping"><spring:message code="label.login.RememberMe"/></label>
                        </p>

                        <p class="login button">
                            <input type="submit" value="Login"/>
                        </p>

                        <p class="change_link">
                            Not a member yet ?
                            <a href="#toregister" class="to_register">Join us</a>
                        </p>
                    </form>
                </div>

                <div id="register" class="animate form">
                    <form:form method="post" action="users/add" commandName="user">
                        <h1> Sign up </h1>

                        <p>
                            <label for="usernamesignup" class="uname" data-icon="u"><spring:message
                                    code="label.login.UserLogin"/></label>
                            <form:input id="usernamesignup" name="usernamesignup" required="required" type="text"
                                        placeholder="mysuperusername690" path="username"/>
                        </p>

                        <p>
                            <label for="emailsignup" class="youmail" data-icon="e"> <spring:message
                                    code="label.login.UserEmail"/></label>
                            <form:input id="emailsignup" name="emailsignup" required="required" type="text"
                                        placeholder="mysupermail@mail.com" path="email"/>
                        </p>

                        <p>
                            <label for="passwordsignup" class="youpasswd" data-icon="p"><spring:message
                                    code="label.login.UserPassword"/></label>
                            <form:input id="passwordsignup" name="passwordsignup" required="required" type="password"
                                        placeholder="eg. X8df!90EO" path="password"/>
                        </p>

                        <p class="signin button">
                            <input type="submit" value="Sign up"/>
                        </p>

                        <p class="change_link">
                            Already a member ?
                            <a href="#tologin" class="to_register"> Go and log in </a>
                        </p>
                    </form:form>
                </div>
                <%--<form:form method="post" action="users/add"  commandName="user">--%>
                <%--<table id="addTable">--%>
                <%--<tr>--%>
                <%--<td><form:label path="username">--%>
                <%--Username:--%>
                <%--</form:label></td>--%>
                <%--<td><form:input path="username"/></td>--%>
                <%--</tr>--%>
                <%--<tr>--%>
                <%--<td><form:label path="email">--%>
                <%--Email:--%>
                <%--</form:label></td>--%>
                <%--<td><form:input path="email"/></td>--%>
                <%--</tr>--%>
                <%--<tr>--%>
                <%--<td>--%>
                <%--<form:label path="password">--%>
                <%--Password:--%>
                <%--</form:label>--%>
                <%--</td>--%>
                <%--<td>--%>
                <%--<form:input path="password"/>--%>

                <%--</td>--%>
                <%--</tr>--%>
                <%--<tr>--%>
                <%--<td colspan="2"><input type="submit"--%>
                <%--value="Sign up"/></td>--%>
                <%--</tr>--%>
                <%--</table>--%>
                <%--</form:form>--%>
            </div>
        </div>
    </section>
</div>
</body>

</html>