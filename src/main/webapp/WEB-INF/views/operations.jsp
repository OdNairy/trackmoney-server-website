<%@page import="by.odnairy.trackmoney.entities.Operation" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf8" %>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<jsp:include page="template.jsp"/>
<script type="text/javascript" src="resources/js/jquery.js"></script>
<script type="text/javascript" src="resources/js/datepicker.js"></script>
<script type="text/javascript" src="resources/js/eye.js"></script>
<script type="text/javascript" src="resources/js/utils.js"></script>
<script type="text/javascript" src="resources/js/layout.js"></script>
<script>
    $(document).ready(function () {
        $('#inputDate').datepicker({ dateFormat:'YMD/'});
    });
</script>
<html>
<head>
    <title><spring:message code="label.title.operations"/></title>
</head>
<body>


<div id="container">
    <div id="inner">
        <div id="centerLeft" style="font-size: 12pt;">
            <c:if test="${!empty operationsList}">
                <table class="data">
                    <tr class="operationsHeader">
                        <th><spring:message code="label.operations.number"/></th>
                        <th><spring:message code="label.operations.summ"/></th>
                        <th><spring:message code="label.operations.comment"/></th>
                        <th>&nbsp;</th>
                    </tr>

                    <c:forEach items="${operationsList}" var="operation">
                        <tr>
                            <td class="operationIdCell" style="width: 30px;">${operation.operationId}</td>
                            <td style="width:100px;"
                                    <% Operation oper = (Operation) pageContext.getAttribute("operation");
                                        if (oper.getAmount() > 0) {
                                            out.print("class=\"profit\"");
                                        } else {
                                            out.print("class=\"charges\"");
                                            oper.setAmount(oper.getAmount() * -1);
                                        }
                                    %>

                                    >${operation.amount}</td>
                            <td class="commentCell">${operation.comment}</td>
                            <td><a class="deleteButton" href="operations/delete/${operation.operationId}">x</a></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>
        </div>

        <div id="centerRight" class="operationCenterRight">
            <form:form method="post" action="operations/add" commandName="operation">

                <table id="addOperationTable">
                    <tr>
                        <td><form:label path="amount">
                            Money:
                        </form:label></td>
                        <td><form:input path="amount"/></td>
                    </tr>
                    <tr>
                        <td><form:label path="comment">
                            Comment:
                        </form:label></td>
                        <td><form:input path="comment"/></td>
                    </tr>
                    <tr>
                        <td>
                            <form:label path="date">
                                Date:
                            </form:label>
                        </td>
                        <td>
                            <form:input path="date"/>

                        </td>
                    </tr>
                        <%--<tr>--%>
                        <%--<td><form:label path="usersByUsersUsername">--%>
                        <%--Username:--%>
                        <%--</form:label></td>--%>
                        <%--<td><form:input path="usersByUsersUsername.username"/></td>--%>
                        <%--</tr>--%>
                    <tr>
                        <td colspan="2"><input type="submit"
                                               value="<spring:message code="label.addOperation"/>"/></td>
                    </tr>
                </table>
            </form:form>
        </div>

    </div>

</div>
</body>
</html>