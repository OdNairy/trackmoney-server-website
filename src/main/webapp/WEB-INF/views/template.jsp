<%--
  Created by IntelliJ IDEA.
  User: odnairy
  Date: 5/13/12
  Time: 10:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<link rel="stylesheet" type="text/css" href="resources/css/menu.css">
<link rel="stylesheet" type="text/css" href="resources/css/demo.css">
<link rel="stylesheet" type="text/css" href="resources/css/common.css">

<body>
<%--<div style="margin-top: 10%;background-color: #ffe6b6" id="menuTab">--%>
<%--<a style="margin-left: 20px;" href="contact"> Contact </a>--%>
<%--<a style="margin-left: 20px;" href="<c:url value="/logins" />"> Login</a>--%>
<%--<a style="margin-left: 20px;" href="<c:url value="/index" />"> Index</a>--%>
<%--<a style="margin-left: 20px;" href="<c:url value="/operations" />"> Operation</a>--%>

<%--<div style="float: right; ">--%>
<%--(${loginName})--%>
<%--<a href="<c:url value="/logout" />">Logout</a>--%>
<%--</div>--%>
<%--</div>--%>
<ul class="menu">
    <div class="menuDiv" align="center">
        <li><a href="dashboard">My dashboard</a></li>
        <li><a href="operations">Operations</a></li>
        <li><a href="categories">Categories</a></li>
        <li><a href="addresses">Addresses</a></li>
        <li><a href="account">Account</a>
            <ul>
                <li><a href="account" class="documents">${loginName}</a></li>
                <%--<li><a href="#" class="messages">Messages</a></li>--%>
                <li><a href="logout" class="signout">Sign Out</a></li>
            </ul>

        </li>

    </div>
</ul>


</body>
</html>