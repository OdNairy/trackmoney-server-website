<%--
  Created by IntelliJ IDEA.
  User: odnairy
  Date: 5/31/12
  Time: 5:40 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="template.jsp"/>

<html>
<head>
    <title></title>
</head>
<body>
<div id="container">
    <div id="inner">

        <div id="centerLeft">
            <c:if test="${!empty categories}">
                <table class="data">
                    <tr class="headerRow">
                        <th class="IdHeaderCell">Id</th>
                        <th>Title</th>

                    </tr>
                    <c:forEach items="${categories}" var="category">
                        <tr>
                            <td class="idCell">${category.categoryId}</td>
                            <td class="mainCell">${category.title}</td>
                            <td class="deleteButton"><a href="categories/delete/${category.categoryId}"><spring:message
                                    code="label.delete"/></a></td>
                        </tr>
                    </c:forEach>

                </table>
            </c:if>
            <form:form method="post" action="categories/add" commandName="category">
                <table>
                    <tr class="headerRow">
                        <th>Title</th>
                    </tr>
                    <tr>
                        <td ><form:input style="margin-top: 10px;" path="title"/></td>
                    </tr>
                </table>
                <input type="submit" title="Save" style="margin-top: 5px;"/>
            </form:form>
        </div>
    </div>
</div>
</body>

</html>