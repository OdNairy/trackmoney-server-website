<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="template.jsp"/>
<link rel="stylesheet" type="text/css" href="resources/css/common.css">

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <title><spring:message code="label.title.account"/></title>
</head>
<body>

<h2><spring:message code="label.loginTitle"/></h2>

<div id="centerLeft">
    <b>User:</b> ${loginName}<br>
    <b>Email:</b> ${email}
</div>

<%--<c:if test="${!empty loginList}">--%>
<%--<table class="data">--%>
<%--<tr>--%>
<%--<th><spring:message code="label.firstname"/></th>--%>
<%--<th><spring:message code="label.email"/></th>--%>
<%--<th><spring:message code="label.telephone"/></th>--%>
<%--<th>&nbsp;</th>--%>
<%--</tr>--%>
<%----%>
<%--<c:forEach items="${loginList}" var="login">--%>
<%--<tr>--%>
<%--<td>${login.username}, ${login.password}</td>--%>
<%--<td>${login.email}</td>--%>

<%--<td><a href="delete/${contact.id}"><spring:message code="label.delete"/></a></td>--%>
<%--</tr>--%>
<%--</c:forEach>--%>
<%--</table>--%>


<%--</c:if>--%>

</body>
</html>