package by.odnairy.trackmoney.entities;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 4:17 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "currency")
public class Currency {
    private int currencyId;

    @javax.persistence.Column(name = "currency_id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    private String currencyName;

    @javax.persistence.Column(name = "currency_name", nullable = false, insertable = true, updatable = true, length = 45, precision = 0)
    @Basic
    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    private String currencyShortName;

    @javax.persistence.Column(name = "currency_short_name", nullable = false, insertable = true, updatable = true, length = 45, precision = 0)
    @Basic
    public String getCurrencyShortName() {
        return currencyShortName;
    }

    public void setCurrencyShortName(String currencyShortName) {
        this.currencyShortName = currencyShortName;
    }

    private int rate;

    @javax.persistence.Column(name = "rate", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Basic
    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Currency currency = (Currency) o;

        if (currencyId != currency.currencyId) return false;
        if (rate != currency.rate) return false;
        if (currencyName != null ? !currencyName.equals(currency.currencyName) : currency.currencyName != null)
            return false;
        if (currencyShortName != null ? !currencyShortName.equals(currency.currencyShortName) : currency.currencyShortName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = currencyId;
        result = 31 * result + (currencyName != null ? currencyName.hashCode() : 0);
        result = 31 * result + (currencyShortName != null ? currencyShortName.hashCode() : 0);
        result = 31 * result + rate;
        return result;
    }

    private Collection<Operation> operationsByCurrencyId;

    @OneToMany(mappedBy = "currencyByFkCurrencyId")
    public Collection<Operation> getOperationsByCurrencyId() {
        return operationsByCurrencyId;
    }

    public void setOperationsByCurrencyId(Collection<Operation> operationsByCurrencyId) {
        this.operationsByCurrencyId = operationsByCurrencyId;
    }
}
