package by.odnairy.trackmoney.entities;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 4:17 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "operation")
public class Operation {
    private int operationId;

    @javax.persistence.Column(name = "operation_id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(int operationId) {
        this.operationId = operationId;
    }

    private long amount;

    @javax.persistence.Column(name = "amount", nullable = false, insertable = true, updatable = true, length = 19, precision = 0)
    @Basic
    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    private String comment;

    @javax.persistence.Column(name = "comment", nullable = true, insertable = true, updatable = true, length = 45, precision = 0)
    @Basic
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    private Timestamp date;

    @javax.persistence.Column(name = "date", nullable = true, insertable = true, updatable = true, length = 19, precision = 0)
    @Basic
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Operation operation = (Operation) o;

        if (amount != operation.amount) return false;
        if (operationId != operation.operationId) return false;
        if (comment != null ? !comment.equals(operation.comment) : operation.comment != null) return false;
        if (date != null ? !date.equals(operation.date) : operation.date != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = operationId;
        result = 31 * result + (int) (amount ^ (amount >>> 32));
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    private Category categoryByFkCategoryId;

    @ManyToOne
    public
    @javax.persistence.JoinColumn(name = "fk_category_id", referencedColumnName = "category_id", nullable = false)
    Category getCategoryByFkCategoryId() {
        return categoryByFkCategoryId;
    }

    public void setCategoryByFkCategoryId(Category categoryByFkCategoryId) {
        this.categoryByFkCategoryId = categoryByFkCategoryId;
    }

    private Account accountByFkAccountId;

    @ManyToOne
    public
    @javax.persistence.JoinColumn(name = "fk_account_id", referencedColumnName = "account_id", nullable = false)
    Account getAccountByFkAccountId() {
        return accountByFkAccountId;
    }

    public void setAccountByFkAccountId(Account accountByFkAccountId) {
        this.accountByFkAccountId = accountByFkAccountId;
    }

    private Addressee addresseeByFkAddresseeId;

    @ManyToOne
    public
    @javax.persistence.JoinColumn(name = "fk_addressee_id", referencedColumnName = "addressee_id", nullable = false)
    Addressee getAddresseeByFkAddresseeId() {
        return addresseeByFkAddresseeId;
    }

    public void setAddresseeByFkAddresseeId(Addressee addresseeByFkAddresseeId) {
        this.addresseeByFkAddresseeId = addresseeByFkAddresseeId;
    }

    private Currency currencyByFkCurrencyId;

    @ManyToOne
    public
    @javax.persistence.JoinColumn(name = "fk_currency_id", referencedColumnName = "currency_id", nullable = false)
    Currency getCurrencyByFkCurrencyId() {
        return currencyByFkCurrencyId;
    }

    public void setCurrencyByFkCurrencyId(Currency currencyByFkCurrencyId) {
        this.currencyByFkCurrencyId = currencyByFkCurrencyId;
    }
}
