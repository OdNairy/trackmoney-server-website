package by.odnairy.trackmoney.entities;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 4:17 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "category")
public class Category {
    private int categoryId;

    @javax.persistence.Column(name = "category_id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    private String title;

    @javax.persistence.Column(name = "title", nullable = true, insertable = true, updatable = true, length = 45, precision = 0)
    @Basic
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (categoryId != category.categoryId) return false;
        if (title != null ? !title.equals(category.title) : category.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = categoryId;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }

    private Category categoryByFkParentCategoryId;

    @ManyToOne
    public
    @javax.persistence.JoinColumn(name = "fk_parent_category_id", referencedColumnName = "category_id")
    Category getCategoryByFkParentCategoryId() {
        return categoryByFkParentCategoryId;
    }

    public void setCategoryByFkParentCategoryId(Category categoryByFkParentCategoryId) {
        this.categoryByFkParentCategoryId = categoryByFkParentCategoryId;
    }

    private Collection<Category> categoriesByCategoryId;

    @OneToMany(mappedBy = "categoryByFkParentCategoryId")
    public Collection<Category> getCategoriesByCategoryId() {
        return categoriesByCategoryId;
    }

    public void setCategoriesByCategoryId(Collection<Category> categoriesByCategoryId) {
        this.categoriesByCategoryId = categoriesByCategoryId;
    }

    private Collection<Operation> operationsByCategoryId;

    @OneToMany(mappedBy = "categoryByFkCategoryId")
    public Collection<Operation> getOperationsByCategoryId() {
        return operationsByCategoryId;
    }

    public void setOperationsByCategoryId(Collection<Operation> operationsByCategoryId) {
        this.operationsByCategoryId = operationsByCategoryId;
    }
}
