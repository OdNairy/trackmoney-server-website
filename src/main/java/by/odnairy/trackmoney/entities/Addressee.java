package by.odnairy.trackmoney.entities;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 4:17 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "addressee")
public class Addressee {
    private int addresseeId;

    @javax.persistence.Column(name = "addressee_id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    public int getAddresseeId() {
        return addresseeId;
    }

    public void setAddresseeId(int addresseeId) {
        this.addresseeId = addresseeId;
    }

    private String addresseeName;

    @javax.persistence.Column(name = "addressee_name", nullable = true, insertable = true, updatable = true, length = 90, precision = 0)
    @Basic
    public String getAddresseeName() {
        return addresseeName;
    }

    public void setAddresseeName(String addresseeName) {
        this.addresseeName = addresseeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Addressee addressee = (Addressee) o;

        if (addresseeId != addressee.addresseeId) return false;
        if (addresseeName != null ? !addresseeName.equals(addressee.addresseeName) : addressee.addresseeName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = addresseeId;
        result = 31 * result + (addresseeName != null ? addresseeName.hashCode() : 0);
        return result;
    }

    private Collection<Operation> operationsByAddresseeId;

    @OneToMany(mappedBy = "addresseeByFkAddresseeId")
    public Collection<Operation> getOperationsByAddresseeId() {
        return operationsByAddresseeId;
    }

    public void setOperationsByAddresseeId(Collection<Operation> operationsByAddresseeId) {
        this.operationsByAddresseeId = operationsByAddresseeId;
    }
}
