package by.odnairy.trackmoney.entities;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 4:17 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "account")
public class Account {
    private int accountId;

    @javax.persistence.Column(name = "account_id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    private String accountName;

    @javax.persistence.Column(name = "account_name", nullable = true, insertable = true, updatable = true, length = 45, precision = 0)
    @Basic
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    private String accountType;

    @javax.persistence.Column(name = "account_type", nullable = true, insertable = true, updatable = true, length = 45, precision = 0)
    @Basic
    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (accountId != account.accountId) return false;
        if (accountName != null ? !accountName.equals(account.accountName) : account.accountName != null) return false;
        if (accountType != null ? !accountType.equals(account.accountType) : account.accountType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = accountId;
        result = 31 * result + (accountName != null ? accountName.hashCode() : 0);
        result = 31 * result + (accountType != null ? accountType.hashCode() : 0);
        return result;
    }

    private Users usersByFkUserId;

    @ManyToOne
    public
    @javax.persistence.JoinColumn(name = "fk_user_id", referencedColumnName = "user_id", nullable = false)
    Users getUsersByFkUserId() {
        return usersByFkUserId;
    }

    public void setUsersByFkUserId(Users usersByFkUserId) {
        this.usersByFkUserId = usersByFkUserId;
    }

    private Collection<Operation> operationsByAccountId;

    @OneToMany(mappedBy = "accountByFkAccountId", fetch = FetchType.EAGER)
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    public Collection<Operation> getOperationsByAccountId() {
        return operationsByAccountId;
    }

    public void setOperationsByAccountId(Collection<Operation> operationsByAccountId) {
        this.operationsByAccountId = operationsByAccountId;
    }
}
