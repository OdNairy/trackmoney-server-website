package by.odnairy.trackmoney.autorization;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created with IntelliJ IDEA.
 * Users: odnairy
 * Date: 5/13/12
 * Time: 11:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class Authority implements GrantedAuthority {
    private String authority;

    public Authority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
