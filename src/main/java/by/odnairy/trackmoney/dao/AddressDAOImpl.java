package by.odnairy.trackmoney.dao;

import by.odnairy.trackmoney.entities.Addressee;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 7:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class AddressDAOImpl implements AddressDAO {
    @Autowired
    SessionFactory sessionFactory;

    public List<Addressee> list() {
        return sessionFactory.getCurrentSession().createCriteria(Addressee.class).list();
    }

    public Addressee createCategory(Addressee category) {
        sessionFactory.getCurrentSession().save(category);
        return category;
    }

    public void deleteCategory(Addressee category) {
        sessionFactory.getCurrentSession().delete(category);
    }

    public Addressee findById(Integer id) {
        return (Addressee) sessionFactory.getCurrentSession().get(Addressee.class, id);
    }
}
