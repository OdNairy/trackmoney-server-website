package by.odnairy.trackmoney.dao;

import by.odnairy.trackmoney.entities.Account;
import by.odnairy.trackmoney.entities.Users;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 4:25 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class AccountDAOImpl implements AccountDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public Collection<Account> list() {
        return sessionFactory.getCurrentSession().createCriteria(Account.class).list();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Collection<Account> listForUser(Users user) {
        sessionFactory.openSession();
        Collection<Account> result = user.getAccountsByUserId();
        result.size();
        return result;
    }
}
