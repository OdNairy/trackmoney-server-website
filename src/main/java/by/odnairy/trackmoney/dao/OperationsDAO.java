package by.odnairy.trackmoney.dao;

import by.odnairy.trackmoney.entities.Operation;
import by.odnairy.trackmoney.entities.Users;
import by.odnairy.trackmoney.entities.Account;


import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/17/12
 * Time: 9:51 PM
 * To change this template use File | Settings | File Templates.
 */
public interface OperationsDAO {
    public Collection<Operation> listOfOperations(Account account);

    public void createOperationForUser(Operation operation);

    public void deleteOperation(Integer id);
}
