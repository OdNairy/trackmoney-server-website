package by.odnairy.trackmoney.dao;

import by.odnairy.trackmoney.entities.Addressee;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 7:08 AM
 * To change this template use File | Settings | File Templates.
 */
public interface AddressDAO {
    public List<Addressee> list();

    public Addressee createCategory(Addressee category);

    public void deleteCategory(Addressee category);

    public Addressee findById(Integer id);
}
