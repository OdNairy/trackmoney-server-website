package by.odnairy.trackmoney.dao;

import java.util.List;

import by.odnairy.trackmoney.entities.Contacts;

import by.odnairy.trackmoney.entities.Users;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DAOImpl implements DAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void addContact(Contacts contact) {
        sessionFactory.getCurrentSession().save(contact);
    }

    @SuppressWarnings("unchecked")
    public List<Contacts> listContact() {
        return sessionFactory.getCurrentSession().createQuery("from Contacts ")
                .list();
    }

    public void removeContact(Integer id) {
        Contacts contact = (Contacts) sessionFactory.getCurrentSession().load(Contacts.class, id);
        if (null != contact) {
            sessionFactory.getCurrentSession().delete(contact);
        }
    }
}
