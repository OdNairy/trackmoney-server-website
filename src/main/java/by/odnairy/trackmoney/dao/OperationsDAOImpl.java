package by.odnairy.trackmoney.dao;

import by.odnairy.trackmoney.entities.Account;
import by.odnairy.trackmoney.entities.Operation;
import by.odnairy.trackmoney.entities.Users;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/17/12
 * Time: 10:03 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class OperationsDAOImpl implements OperationsDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public OperationsDAOImpl() {
    }

    @Override
    public Collection<Operation> listOfOperations(Account account) {
        return null;//account.getOperationList();
    }

    @Override
    public void createOperationForUser(Operation operation) {

        sessionFactory.getCurrentSession().save(operation);
    }

    @Override
    public void deleteOperation(Integer id) {
        Operation operation = (Operation) sessionFactory.getCurrentSession().load(Operation.class, id);
        if (null != operation) {
            sessionFactory.getCurrentSession().delete(operation);

        }
    }


    public static int getData() {
        return 1000;

    }
}
