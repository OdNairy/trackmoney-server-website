package by.odnairy.trackmoney.dao;

import by.odnairy.trackmoney.entities.Category;
import by.odnairy.trackmoney.entities.Currency;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 5:30 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CategoryDAOImpl implements CategoryDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    public List<Category> list() {
        try {
            return sessionFactory.getCurrentSession().createCriteria(Category.class).list();
        } catch (Exception e) {
            System.out.print("Can't get list of Category. Reason: " + e.getLocalizedMessage());
        }
        return null;
    }

    public Category createCategory(Category category) {
        sessionFactory.getCurrentSession().save(category);
        return category;
    }

    public void deleteCategory(Category category) {
        sessionFactory.getCurrentSession().delete(category);
    }

    public Category findById(Integer id) {
        return (Category) sessionFactory.getCurrentSession().get(Category.class, id);
    }

    public Currency currencyById(Integer id) {
        return (Currency) sessionFactory.getCurrentSession().get(Currency.class, id);
    }
}
