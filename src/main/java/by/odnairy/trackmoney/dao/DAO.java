package by.odnairy.trackmoney.dao;

import java.util.List;

import by.odnairy.trackmoney.entities.Contacts;
import by.odnairy.trackmoney.entities.Users;

public interface DAO {

    public void addContact(Contacts contact);

    public List<Contacts> listContact();

    public void removeContact(Integer id);
}