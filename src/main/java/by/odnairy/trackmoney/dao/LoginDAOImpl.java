package by.odnairy.trackmoney.dao;

import by.odnairy.trackmoney.entities.Authorities;
import by.odnairy.trackmoney.entities.Users;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Users: odnairy
 * Date: 5/13/12
 * Time: 12:35 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class LoginDAOImpl implements LoginDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addLogin(Users user) {
        user.setEnabled(true);

//        ArrayList<Authorities> roles = new ArrayList<Authorities>();
//        roles.add(new Authorities("ROLE_USER"));
//        user.setAuthoritiesesByUsername(roles);

        Authorities role = new Authorities();
        //role.setUsersByUsername(user);
        role.setAuthority("ROLE_USER");

        sessionFactory.getCurrentSession().save(role);

        sessionFactory.getCurrentSession().save(user);

    }


    @SuppressWarnings("unchecked")
    public List<Users> list() {
        return sessionFactory.getCurrentSession().createCriteria(Users.class).list();
    }

    @Override
    public void removeLogin(Integer id) {
        Users login = (Users) sessionFactory.getCurrentSession().load(Users.class, id);
        if (null != login) {
            sessionFactory.getCurrentSession().delete(login);
        }
    }

    @Override
    public Users retrieveLogin(String username) {
//        return (Users) sessionFactory.getCurrentSession().get(Users.class, username);  //To change body of implemented methods use File | Settings | File Templates.
        List<Users> users = this.list();
        Users user = null;
        for (Users usr : users) {
            if (username.equalsIgnoreCase(usr.getUsername())) {
                return usr;
            }
        }
        System.out.print("Can not find user with name: " + username);
        return list().get(0);
    }
}
