package by.odnairy.trackmoney.dao;

import by.odnairy.trackmoney.entities.Account;
import by.odnairy.trackmoney.entities.Users;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 4:24 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AccountDAO {
    public Collection<Account> list();

    public Collection<Account> listForUser(Users user);
}
