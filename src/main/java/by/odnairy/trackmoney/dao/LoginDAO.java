package by.odnairy.trackmoney.dao;

import by.odnairy.trackmoney.entities.Users;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Users: odnairy
 * Date: 5/13/12
 * Time: 12:35 PM
 * To change this template use File | Settings | File Templates.
 */
public interface LoginDAO {

    public void addLogin(Users login);

    public List<Users> list();

    public Users retrieveLogin(String username);

    public void removeLogin(Integer id);

}
