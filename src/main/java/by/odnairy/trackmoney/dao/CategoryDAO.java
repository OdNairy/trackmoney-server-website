package by.odnairy.trackmoney.dao;

import by.odnairy.trackmoney.entities.Category;
import by.odnairy.trackmoney.entities.Currency;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 5:28 AM
 * To change this template use File | Settings | File Templates.
 */
public interface CategoryDAO {
    public List<Category> list();

    public Category createCategory(Category category);

    public Category findById(Integer id);

    public void deleteCategory(Category category);

    public Currency currencyById(Integer id);
}
