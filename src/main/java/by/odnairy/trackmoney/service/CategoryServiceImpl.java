package by.odnairy.trackmoney.service;

import by.odnairy.trackmoney.dao.CategoryDAO;
import by.odnairy.trackmoney.entities.Category;
import by.odnairy.trackmoney.entities.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 5:35 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryDAO categoryDAO;


    @Transactional
    public List<Category> list() {
        return categoryDAO.list();
    }

    @Transactional
    public Category createCategory(Category category) {
        return categoryDAO.createCategory(category);
    }

    @Transactional
    public void deleteCategory(Category category) {
        categoryDAO.deleteCategory(category);
    }

    @Transactional
    public Category findById(Integer id) {
        return categoryDAO.findById(id);
    }

    @Transactional
    public Currency currencyById(Integer id) {
        return categoryDAO.currencyById(id);
    }
}
