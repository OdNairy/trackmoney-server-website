package by.odnairy.trackmoney.service;

import by.odnairy.trackmoney.dao.AddressDAO;
import by.odnairy.trackmoney.entities.Addressee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 7:22 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class AddressServiceImpl implements AddressService {
    @Autowired
    AddressDAO addressDAO;

    @Transactional
    public List<Addressee> list() {
        return addressDAO.list();
    }

    @Transactional
    public Addressee createCategory(Addressee category) {
        return addressDAO.createCategory(category);
    }

    @Transactional
    public void deleteCategory(Addressee category) {
        addressDAO.deleteCategory(category);
    }

    @Transactional
    public Addressee findById(Integer id) {
        return addressDAO.findById(id);
    }
}
