package by.odnairy.trackmoney.service;

import by.odnairy.trackmoney.entities.Operation;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/22/12
 * Time: 10:56 AM
 * To change this template use File | Settings | File Templates.
 */
public interface OperationsService {
    public void createOperation(Operation operation);

    public void deleteOperation(Integer id);
}
