package by.odnairy.trackmoney.service;

import java.util.List;

import by.odnairy.trackmoney.entities.Contacts;

public interface ContactService {

    public void addContact(Contacts contact);

    public List<Contacts> listContact();

    public void removeContact(Integer id);
}
