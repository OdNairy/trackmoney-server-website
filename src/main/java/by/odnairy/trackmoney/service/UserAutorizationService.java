package by.odnairy.trackmoney.service;

import by.odnairy.trackmoney.dao.LoginDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created with IntelliJ IDEA.
 * Users: odnairy
 * Date: 5/13/12
 * Time: 11:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserAutorizationService implements UserDetailsService {
    @Autowired
    LoginDAO loginDAO;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        return (UserDetails) loginDAO.retrieveLogin(username);  //To change body of implemented methods use File | Settings | File Templates.
    }
}
