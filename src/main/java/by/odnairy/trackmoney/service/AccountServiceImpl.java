package by.odnairy.trackmoney.service;

import by.odnairy.trackmoney.dao.AccountDAO;
import by.odnairy.trackmoney.entities.Account;
import by.odnairy.trackmoney.entities.Operation;
import by.odnairy.trackmoney.entities.Users;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 4:27 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    AccountDAO accountDAO;

    @Transactional

    public Collection<Account> list() {
        return accountDAO.list();
    }

    @Transactional
    public Collection<Operation> listForAccount(Account account) {
        return account.getOperationsByAccountId();
    }

    @Transactional
    public Collection<Account> listForUser(Users user) {


        return accountDAO.listForUser(user);
    }
}
