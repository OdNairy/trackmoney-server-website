package by.odnairy.trackmoney.service;

import by.odnairy.trackmoney.entities.Users;
import by.odnairy.trackmoney.dao.LoginDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginDAO loginDAO;

    @Transactional
    public void addLogin(Users login) {

        loginDAO.addLogin(login);
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Transactional
    public List<Users> list() {
        return loginDAO.list();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Transactional
    public void removeLogin(Integer id) {
        loginDAO.removeLogin(id);
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Transactional
    public Users retrieveLogin(String username) {
        return loginDAO.retrieveLogin(username);
    }
}
