package by.odnairy.trackmoney.service;

import by.odnairy.trackmoney.entities.Operation;
import by.odnairy.trackmoney.dao.OperationsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/22/12
 * Time: 10:57 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class OperationsServiceImpl implements OperationsService {

    @Autowired
    private OperationsDAO operationsDAO;

    @Transactional
    public void createOperation(Operation operation) {
        operationsDAO.createOperationForUser(operation);
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Transactional
    public void deleteOperation(Integer id) {
        operationsDAO.deleteOperation(id);
    }
}