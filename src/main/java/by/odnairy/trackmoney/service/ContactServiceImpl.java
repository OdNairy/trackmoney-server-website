package by.odnairy.trackmoney.service;

import by.odnairy.trackmoney.dao.DAO;
import by.odnairy.trackmoney.entities.Contacts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    private DAO DAO;

    @Transactional
    @Override
    public void addContact(Contacts contact) {
        DAO.addContact(contact);
    }

    @Transactional
    @Override
    public List<Contacts> listContact() {

        return DAO.listContact();
    }

    @Transactional
    @Override
    public void removeContact(Integer id) {
        DAO.removeContact(id);
    }
}
