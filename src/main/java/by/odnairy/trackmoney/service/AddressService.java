package by.odnairy.trackmoney.service;

import by.odnairy.trackmoney.entities.Addressee;
import by.odnairy.trackmoney.entities.Category;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 7:11 AM
 * To change this template use File | Settings | File Templates.
 */
public interface AddressService {
    public List<Addressee> list();

    public Addressee createCategory(Addressee category);

    public Addressee findById(Integer id);

    public void deleteCategory(Addressee category);
}
