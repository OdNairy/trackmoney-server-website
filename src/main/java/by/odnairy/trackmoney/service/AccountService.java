package by.odnairy.trackmoney.service;

import by.odnairy.trackmoney.entities.Account;
import by.odnairy.trackmoney.entities.Operation;
import by.odnairy.trackmoney.entities.Users;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: odnairy
 * Date: 5/31/12
 * Time: 4:27 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AccountService {
    public Collection<Account> list();

    public Collection<Operation> listForAccount(Account account);

    public Collection<Account> listForUser(Users user);
}
