package by.odnairy.trackmoney.web;

import by.odnairy.trackmoney.dao.AccountDAO;
import by.odnairy.trackmoney.dao.AddressDAO;
import by.odnairy.trackmoney.dao.CategoryDAO;
import by.odnairy.trackmoney.entities.*;
import by.odnairy.trackmoney.service.*;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


@Controller
@SessionAttributes({"loginName", "email"})
public class RootController {

    @Autowired
    private ContactService contactService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private OperationsService operationsService;

    @RequestMapping("/")
    public String home() {
        return "redirect:/index";
    }

    @RequestMapping("/index")
    public String index() {
        return "redirect:/dashboard";
    }


    @RequestMapping("dashboard")
    public String dashboard(ModelMap map) {
        String username = (String) map.get("loginName");
        if (null == username) {
            username = getUsername();
            map.addAttribute("loginName", username);
        }

        List<Addressee> addressees = addressService.list();
        List<Category> categories = categoryService.list();

        Users user = loginService.retrieveLogin(getUsername());
        Collection<Account> accounts = accountService.list();//accountService.listForUser(user);
        Account account = accounts.iterator().next();

        Collection<Operation> operations = account.getOperationsByAccountId();

        Integer income = 0;
        Integer charges = 0;

        for(Operation operation: operations){
            if (operation.getAmount()>0)
                income+=operation.getAmount();
            else
                charges+=operation.getAmount();
        }

        map.addAttribute("incomeSumm",income);
        map.addAttribute("chargesSumm",charges);
//        Hibernate.initialize(accounts.size());



        map.addAttribute("operationsList", operations);
        map.addAttribute("categories", categories);
        map.addAttribute("addresses", addressees);


        return "dashboard";
    }

    @RequestMapping("/contact")
    public String listContacts(Map<String, Object> map, @ModelAttribute("loginName") String login) {

        map.put("contact", new Contacts());
        map.put("contactList", contactService.listContact());

        return "contact";
    }


    @RequestMapping("account")
    public String account(ModelMap map) {
//        Users user = loginService.retrieveLogin(getUsername());
//        map.addAttribute("email", user.getEmail());

        return "account";
    }

    @Autowired
    AccountService accountService;

    @RequestMapping("operations")
    public String operationsList(ModelMap map) {

//        ArrayList<Operation> lists = (ArrayList<Operation>)operations;

        Users user = loginService.retrieveLogin(getUsername());
        Collection<Account> accounts = accountService.list();//accountService.listForUser(user);

        Hibernate.initialize(accounts.size());

        Account account = accounts.iterator().next();

        map.addAttribute("operationsList", account.getOperationsByAccountId());
        Operation operation = new Operation();
        map.addAttribute("operation", operation);

        return "operations";
    }

    @RequestMapping(value = "operations/add", method = RequestMethod.POST)
    public String addOperation(@ModelAttribute("operation") Operation operation, BindingResult result) {
        Users user = loginService.retrieveLogin(getUsername());
        Collection<Account> accounts = accountService.list();//accountService.listForUser(user);
        Addressee adrs = addressService.list().iterator().next();
        Category categore = categoryService.findById(1);
        Currency cur = categoryService.currencyById(1);

        Hibernate.initialize(accounts.size());

        Account account = accounts.iterator().next();
        operation.setAccountByFkAccountId(account);
        operation.setAddresseeByFkAddresseeId(adrs);
        operation.setCurrencyByFkCurrencyId(cur);
        operation.setCategoryByFkCategoryId(categore);

// );

        operationsService.createOperation(operation);
        return "redirect:/operations";
    }

    @RequestMapping("/operations/delete/{operationId}")
    public String deleteOperation(@PathVariable("operationId") Integer operationId) {
        operationsService.deleteOperation(operationId);
        return "redirect:/operations";
    }

    @RequestMapping("login")
    public String login(ModelMap map) {
        map.addAttribute("user", new Users());
        return "login";
    }


    @RequestMapping(value = "/contact/add", method = RequestMethod.POST)
    public String addContact(@ModelAttribute("contact") Contacts contact,
                             BindingResult result) {

        contactService.addContact(contact);

        return "redirect:/contact";
    }

    @RequestMapping("/contact/delete/{contactId}")
    public String deleteContact(@PathVariable("contactId") Integer contactId) {
        contactService.removeContact(contactId);
        return "redirect:/contact";
    }

    public Object getPrincipal() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (null == auth)
            return null;
        Object obj = auth.getPrincipal();
        return obj;
    }

    public String getUsername() {
        Object obj = getPrincipal();
        String username = null;
        if (obj instanceof UserDetails) {
            username = ((UserDetails) obj).getUsername();
        } else {
            username = obj.toString();
        }

        return username;
    }

    public List<String> getAuthorities() {
        Object obj = getPrincipal();
        List<String> authorities = new ArrayList<String>();

        authorities.add("ROLE_ANONYMOUS");

        if (obj instanceof UserDetails) {
            authorities.remove(0);
            Collection<GrantedAuthority> collection = ((UserDetails) obj).getAuthorities();
            for (GrantedAuthority role : collection) {
                authorities.add(role.getAuthority());
            }
        }

        return authorities;
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.POST)
    public String registerUser(@ModelAttribute("user") Users user,
                               BindingResult result) {
        loginService.addLogin(user);

        return "redirect:/login";
    }


    @Autowired
    CategoryService categoryService;

    @RequestMapping("/categories")
    public String categories(ModelMap map) {
        List<Category> list = categoryService.list();

        map.addAttribute("categories", list);
        map.addAttribute("category", new Category());
        return "categories";
    }

    @RequestMapping("/categories/delete/{categoryId}")
    public String deleteCategory(@PathVariable("categoryId") Integer categoryId) {
        categoryService.deleteCategory(categoryService.findById(categoryId));
        return "redirect:/categories";
    }

    @RequestMapping("/categories/add")
    public String addCategory(@ModelAttribute("category") Category category, BindingResult result) {
        categoryService.createCategory(category);
        return "redirect:/categories";
    }


    @Autowired
    AddressService addressService;

    @RequestMapping("/addresses")
    public String addresses(ModelMap map) {
        List<Addressee> list = addressService.list();
        map.addAttribute("addresses", list);
        map.addAttribute("addressee", new Addressee());

        return "addresses";
    }

    @RequestMapping("/addresses/delete/{addresseeId}")
    public String deleteAddressee(@PathVariable("addresseeId") Integer categoryId) {
        addressService.deleteCategory(addressService.findById(categoryId));
        return "redirect:/addresses";
    }

    @RequestMapping("/addresses/add")
    public String addAddressee(@ModelAttribute("addresses") Addressee addressee, BindingResult result) {
        addressService.createCategory(addressee);
        return "redirect:/addresses";
    }
}
